package com.nkming.nc_photos

interface K {
	companion object {
		const val DOWNLOAD_NOTIFICATION_ID_MIN = 1000;
		const val DOWNLOAD_NOTIFICATION_ID_MAX = 2000;
	}
}
